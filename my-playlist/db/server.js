// server.js
const jsonServer = require('json-server');
let server, router, middlewares;

function startServer() {
  server = jsonServer.create();
  router = jsonServer.router('./db/db.json');
  middlewares = jsonServer.defaults();
  server.use(middlewares)
  server.use(router)
  return server.listen(9000, () => {
    console.log('JSON Server is running on port 9000')
  });
};

module.exports = {
  server: server,
  startServer: startServer,
};