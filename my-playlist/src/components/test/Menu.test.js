import React from "react";
import ReactDOM from "react-dom";
import {
  MemoryRouter
} from "react-router-dom";
import Menu from "../Menu";
import { mount } from 'enzyme';
import { NavItem } from 'reactstrap';

describe("Menu", function () {
  it("it should render 2 menu items", function () {
    const wrapper = mount(
      <MemoryRouter>
        <Menu />
      </MemoryRouter>
    );
    const menuItems = wrapper.find(NavItem);
    expect(menuItems).toHaveLength(2);
  });

});
