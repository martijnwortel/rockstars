import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter } from "react-router-dom";
import About from "../About";

describe("About", function () {
  it("it should render", function () {
    const div = document.createElement("div");
    ReactDOM.render(
      <MemoryRouter>
        <About />
      </MemoryRouter>,
      div
    );
  });
});
