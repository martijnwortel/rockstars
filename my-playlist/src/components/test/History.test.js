import React from "react";
import ReactDOM from "react-dom";
import {
  MemoryRouter
} from "react-router-dom";
import History from "../History";
import { mount } from 'enzyme';
import { Button } from 'reactstrap';

describe("History", function () {
  it("it should render title property", function () {
    const wrapper = mount(
      <MemoryRouter>
        <History backbtnTitle="playlist" />
      </MemoryRouter>
    );

    const backBtn = wrapper.find(Button);
    expect(backBtn.text()).toEqual('playlist');
  });
  it("it should render a default text", function () {
    const wrapper = mount(
      <MemoryRouter>
        <History />
      </MemoryRouter>
    );

    const backBtn = wrapper.find(Button);
    expect(backBtn.text()).toEqual('Back');
  });
});
