import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav, NavItem, NavLink, Row, Col } from "reactstrap";

export default class Menu extends Component {
  constructor(props) {
    super(props);
    this.isActive = this.isActive.bind(this);
  }
  isActive(location) {
    if (location === this.props.urlLocation) return "active";
    return "";
  }
  render() {
    return (
      <Row>
        <Col>
          <Navbar>
            <Nav className="navbar-expand-lg navbar" pills>
              <NavItem>
                <Link className={`nav-link ${this.isActive("/")}`} to="/">
                  Home
                </Link>
              </NavItem>
              <NavItem>
                <Link
                  to="/playlists"
                  className={`nav-link ${this.isActive("/playlists")}`}>
                  My playlists ({this.props.playlistsCount})
                </Link>
              </NavItem>
            </Nav>
          </Navbar>
        </Col>
      </Row>
    );
  }
}
