import React, { Component } from "react";
import PlaylistBtn from "./PlaylistBtn";
import Octicon from "react-octicon";
export default class Song extends Component {
  constructor(props) {
    super(props);

    this.state = {
      inPlaylist: false
    };

    this.playlistBtnClicked = this.playlistBtnClicked.bind(this);
  }

  playlistBtnClicked(data) {
    this.props.toggleSongInPlaylist(this.props.song, this.props.playlistIndex);
  }

  render() {
    return (
      <tbody>
        <tr>
          <td>{this.props.song.artist}</td>
          <td>{this.props.song.name}</td>
          <td className="text-right">
            <PlaylistBtn
              add={!(this.props.playlistIndex > -1)}
              btnEvent={this.playlistBtnClicked}
            />
          </td>
        </tr>
      </tbody>
    );
  }
}
