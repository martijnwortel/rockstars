import React, { Component } from "react";
import { Button } from "reactstrap";
import Octicon from "react-octicon";
export default class PlaylistBtn extends Component {
  constructor(props) {
    super(props);
    this.btnClicked = this.btnClicked.bind(this);
  }
  btnClicked(event) {
    event.preventDefault();
    event.stopPropagation();

    if (this.props.btnEvent) {
      this.props.btnEvent(this);
    }
  }
  render() {
    return (
      <div>
        {!!this.props.add && (
          <Button outline color="primary" onClick={this.btnClicked}>
            Add
            <Octicon mega name="plus" className="align-middle" />
          </Button>
        )}
        {!this.props.add && (
          <Button outline color="warning" onClick={this.btnClicked}>
            Remove
            <Octicon mega name="x" className="align-middle" />
          </Button>
        )}
      </div>
    );
  }
}
