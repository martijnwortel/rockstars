import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Navbar, Nav, NavItem, NavLink } from "reactstrap";

export default class PlaylistInfo extends Component {
  render() {
    return (
      <Link to="/playlist" className={this.props.className}>
        Current playlist ({this.props.songCount})
      </Link>
    );
  }
}
