import React, { Component } from "react";
import { Link } from "react-router-dom";

import { Jumbotron, Button } from "reactstrap";

export default class About extends Component {
  render() {
    return (
      <div>
        <Jumbotron>
          <h1 className="display-3">Welcome!</h1>
          <p className="lead">Bla die bla die bla</p>
          <hr className="my-2" />

          <p className="lead">
            <Link to="/playlists">Get started with making your playlists!</Link>
          </p>
        </Jumbotron>
      </div>
    );
  }
}
