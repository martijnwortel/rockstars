import React, { Component } from "react";
import { Button, Link } from "reactstrap";
import { withRouter } from "react-router-dom";

class History extends Component {
  constructor(props) {
    super(props);
    this.backInHistory = this.backInHistory.bind(this);
  }
  backInHistory() {
    this.props.history.goBack();
  }
  render() {
    return (
      <div>
        <Button color="link" onClick={this.backInHistory}>
          {this.props.backbtnTitle || "Back"}
        </Button>
      </div>
    );
  }
}
export default withRouter(History);
