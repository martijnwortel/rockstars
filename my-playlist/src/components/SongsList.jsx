import React, { Component } from "react";
import Song from "./Song";
import { Row, Col, Button } from "reactstrap";
import { Link, withRouter } from "react-router-dom";
import { Table } from "reactstrap";

class SongsList extends Component {
  constructor(props) {
    super(props);
    this.songsList = this.songsList.bind(this);
    this.songPlaylistIndex = this.songPlaylistIndex.bind(this);
  }
  songPlaylistIndex(song) {
    let inPlaylist = -1;
    this.props.playlist.forEach((item, index) => {
      if (item.id === song.id) inPlaylist = index;
    });
    return inPlaylist;
  }

  songsList() {
    return this.props.songs.map(song => {
      return (
        <Song
          key={song.name}
          song={song}
          playlistIndex={this.songPlaylistIndex(song)}
          toggleSongInPlaylist={this.props.toggleSongInPlaylist}
        />
      );
    });
  }
  render() {
    return (
      <Table borderless className="table-hover">
        <thead>
          <tr>
            <th>Artist</th>
            <th>Name</th>
            <th className="text-right">
              {!!this.props.showAddSongsBtn && (
                <Button
                  color="primary"
                  tag={Link}
                  to={`${this.props.match.url}/add`}>
                  Add great songs!
                </Button>
              )}
            </th>
          </tr>
        </thead>
        {this.songsList()}
      </Table>
    );
  }
}

export default withRouter(SongsList);
