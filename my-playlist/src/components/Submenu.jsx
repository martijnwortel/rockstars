import React, { Component } from "react";
import { Button, Nav, Navbar, NavItem } from "reactstrap";

export default class Submenu extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Navbar className="navbar-expand-lg">
        <Nav className="ml-auto" navbar>
          {this.props.children}
        </Nav>
      </Navbar>
    );
  }
}
