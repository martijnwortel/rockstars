import React, { Component } from "react";
import { Row, Col, Button, Form, FormGroup, Label, Input } from "reactstrap";

const titleField = {
  fontSize: "1.5em",
  height: "1.3em",
  width: "auto",
  display: "inline-block"
};
export default class NameAndEdit extends Component {
  _timeoutID;

  constructor(props) {
    super(props);
    this._onBlur = this._onBlur.bind(this);
    this.state = {
      name: "",
      isManagingFocus: false
    };
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.initialName !== this.props.initialName) {
      this.setState({
        name: this.props.initialName
      });
    }
  }

  componentWillMount() {
    this.setState({
      name: this.props.initialName
    });
  }
  _onBlur() {
    this._timeoutID = setTimeout(() => {
      if (this.props.afterEditEvent) this.props.afterEditEvent(this.state.name);
      // if (this.state.isManagingFocus) {
      //   this.setState({
      //     isManagingFocus: false
      //   });
      // }
    }, 0);
  }
  render() {
    return (
      <div>
        <div>Playlist name:</div>
        <Input
          style={titleField}
          type="text"
          name="name"
          id="name"
          value={this.state.name}
          onChange={event => this.setState({ name: event.target.value })}
          onBlur={this._onBlur}
        />
      </div>
    );
  }
}
