const puppeteer = require('puppeteer');

describe('e2e new playlist', () => {
  test('e2e Make 2 new playlists', async () => {
    let browser = await puppeteer.launch({
      headless: false
    });
    let page = await browser.newPage();

    page.emulate({
      viewport: {
        width: 500,
        height: 2400
      },
      userAgent: ''
    });

    await page.goto('http://localhost:3000/playlists', {
      waitUntil: 'networkidle2'
    });

    await page.click('.btn-secondary');
    await page.click('.btn-secondary');

    let playlists;
    await page.evaluate(() => {
      playlists = document.querySelectorAll('.table-borderless tbody tr').length;
      return playlists
    }).then((pls) => {
      expect(pls).toBe(2);
    })

    // await browser.close();
  }, 16000);
});