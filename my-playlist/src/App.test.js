import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './containers/App';
import { MemoryRouter } from "react-router-dom";
import { mount } from 'enzyme';

import { createStore, compose, applyMiddleware } from "redux";
import reducers from "./reducers";
import { fetchArtists, fetchSongs } from "./actions/index";


const store = createStore(
    reducers
);

it('renders without crashing and finds logo Rockstars', () => {
    const wrapper = mount(
        <MemoryRouter>
            <App store={store} fetchArtists={fetchArtists} fetchSongs={fetchSongs} />
        </MemoryRouter>
    );
    const logo = wrapper.find('.rockstars');
    expect(logo.length).toEqual(1);
});
