import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
// import "./index.css";

import { Provider } from "react-redux";
import { createStore, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import ReduxPromise from "redux-promise";
import { ConnectedRouter, routerMiddleware } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import reducers from "./reducers";
import App from "./containers/App";
import persistState from "redux-localstorage";
// import registerServiceWorker from"./registerServiceWorker";

const history = createHistory();
const middlewareHistory = routerMiddleware(history);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middlewares = applyMiddleware(thunk, ReduxPromise, ...middlewareHistory);

persistState();

const store = createStore(
  reducers,
  composeEnhancers(persistState(["Playlists"]), middlewares)
);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,

  document.getElementById("root")
);

{
  /* registerServiceWorker(); */
}
