import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
// const ReactRouter = require("react-router");
// let { Router, Route, Link } = ReactRouter;

import App from "./App";
import About from "./components/About";
import Artists from "./containers/Artists";

export default () => {
  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/artists">Artists</Link>
          </li>
        </ul>
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/about" component={About} />
          <Route path="/artists" component={Artists} />
        </Switch>
      </div>
    </Router>
  );
};

// ReactDOM.render(
//
// export default () => {
//   return (
//     <Router>
//       <Route path="/" component={Main} />
//       {/* <Route path="/about" component={About} /> */}
//       {/* <Route path="/posts" component={Posts} posts={posts} /> */}
//       {/* <Route path="/posts/:id" component={Post} posts={posts} />
//         <Route path="/contact" component={Contact} /> */}
//       {/* </Route> */}
//       {/* <Route path="/login" component={Login} /> */}
//     </Router>
//   );
// };
