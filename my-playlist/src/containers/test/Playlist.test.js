import renderer from "react-test-renderer";
import React from "react";
import {
  MemoryRouter
} from "react-router-dom";
import {
  Playlist,
  mapStateToProps
} from "../Playlist";

import openPlaylist from 'openPlaylist';
import { mount } from 'enzyme';


describe("Playlist", function () {

  const history = {
    push: jest.fn((data) => {
      if (data) return data
      return undefined
    })
  };
  const songTemplate = {
    "id": 1,
    "name": "3 Doors Down"
  };
  const playlistTemplate = { id: 1, name: 'mijn playlist', songs: [songTemplate] };

  it("it should return home with no playlist id", function () {
    const div = document.createElement("div");
    const mockAppfn = jest.fn();

    const matchProps = {
      params: {
        playlist: -1
      }
    };
    const playlistState = {
      id: -1, name: '', songs: [songTemplate]
    };
    const wrapper = mount(
      <MemoryRouter initialEntries={['/playlist/jl4xuvn0', { pathname: '/playlist/jl4xuvn0' }]}>
        <Playlist history={history} match={matchProps} playlist={playlistState} />
      </MemoryRouter>
    );
    expect(history.push).toHaveBeenCalledTimes(1)
  });

  it("it should render a playlist with 3 doors down a make a snapshot", function () {

    const matchProps = {
      params: {
        playlist: 1
      }
    }
    const playlistState = {
      id: -1, name: '', songs: [songTemplate]
    }
    const tree = renderer.create(
      <MemoryRouter initialEntries={['/playlist/jl4xuvn0', { pathname: '/playlist/jl4xuvn0' }]}>
        <Playlist history={history} match={matchProps} playlist={playlistState} />
      </MemoryRouter>
    );

    expect(tree.toJSON()).toMatchSnapshot();

  });

  it("should map the state to props correctly", () => {
    const sampleState = {
      playlist: playlistTemplate,
      playlists: [playlistTemplate]
    };
    const appState = {
      Playlist: playlistTemplate,
      Playlists: [playlistTemplate]
    };
    const componentState = mapStateToProps(appState);
    expect(componentState).toEqual(sampleState);
  });
});