import renderer from "react-test-renderer";
import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter } from "react-router-dom";
import { App, mapStateToProps } from "../App";
import { fetchArtists, fetchSongs } from "../../actions/index";

describe("App", function () {
  describe("mapsStateToProps", () => {
    it("should map the state to props correctly", () => {
      const sampleState = {
        playlistsLength: 1,
        location: "playlist/jl4xuvn0"
      };
      const appState = {
        Playlists: [{}],
        routerReducer: {
          location: { pathname: "playlist/jl4xuvn0" }
        }
      };
      const componentState = mapStateToProps(appState);
      expect(componentState).toEqual(sampleState);
    });
  });

  it("it should render and match snapshot", function () {
    const div = document.createElement("div");
    const mockAppfn = jest.fn();

    const tree = renderer.create(
      <MemoryRouter>
        <App fetchArtists={fetchArtists} fetchSongs={fetchSongs} />
      </MemoryRouter>
    );

    expect(tree.toJSON()).toMatchSnapshot();
    // ReactDOM.render(
    //   <MemoryRouter>
    //     <App fetchArtists={fetchArtists} fetchSongs={fetchSongs} />
    //   </MemoryRouter>,
    //   div
    // );
  });
});
