import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import logo from "../logo-rockstars_1.png";
import "../rockstars.css";

import { Route, Switch, Link, withRouter } from "react-router";

import About from "../components/About";
import Menu from "../components/Menu";
import Playlist from "../containers/Playlist";
import Playlists from "../containers/Playlists";

import { fetchArtists, fetchSongs } from "../actions/index";
import { Container, Row, Col } from "reactstrap";
import headerImg from "../header_awards.jpg";
const headerStyle = {
  backgroundImage: "url(" + headerImg + ")"
};

export class App extends Component {
  constructor(props) {
    super(props);
  }
  componentWillMount() {
    this.props.fetchArtists();
    this.props.fetchSongs();
  }
  render() {
    return (
      <div className="App">
        <header className="App-header" style={headerStyle}>
          <img src={logo} className="App-logo rockstars" alt="logo" />
        </header>
        <Container>
          <Menu
            playlistsCount={this.props.playlistsLength}
            urlLocation={this.props.location}
          />
          <Switch>
            <Route path="/" component={About} exact={true} />
            <Route path="/playlist/:playlist" component={Playlist} />
            <Route path="/playlists" component={Playlists} />
          </Switch>
        </Container>
      </div>
    );
  }
}

export function mapStateToProps(state) {
  return {
    playlistsLength: state.Playlists.length,
    location: state.routerReducer.location.pathname
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchArtists, fetchSongs }, dispatch);
}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(App)
);
