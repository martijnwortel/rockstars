import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { Row, Col, Table, Button, NavItem } from "reactstrap";
import PlaylistBtn from "../components/PlaylistBtn";
import Submenu from "../components/Submenu";

import { removePlaylist, newPlaylist, openPlaylist } from "../actions/index";

class Playlists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: ""
    };
    this.playlists = this.playlists.bind(this);
    this.newPlaylist = this.newPlaylist.bind(this);
    this.removePlaylist = this.removePlaylist.bind(this);
  }
  openPlaylist(plIndex) {
    this.props.openPlaylist(this.props.playlists[plIndex]);
  }
  newPlaylist() {
    this.props.newPlaylist(this.props.playlists.length);
  }
  removePlaylist(data) {
    this.props.removePlaylist(data.props.index);
  }
  playlists() {
    return this.props.playlists.map((playlist, index) => {
      return (
        <tr key={index}>
          <td>
            <Link
              to={`/playlist/${playlist.id}`}
              onClick={this.openPlaylist.bind(this, index)}>
              {playlist.name}
            </Link>
          </td>
          <td>{playlist.songs.length}</td>
          <td className="text-right">
            <PlaylistBtn
              add={false}
              btnEvent={this.removePlaylist}
              index={index}
            />
          </td>
        </tr>
      );
    });
  }
  render() {
    return (
      <Row>
        <Col>
          <h1>Playlists</h1>
          <Submenu>
            <NavItem>
              <Button onClick={this.newPlaylist}>New playlist</Button>
            </NavItem>
          </Submenu>
        </Col>
        <Table borderless>
          <thead>
            <tr>
              <th>Name</th>
              <th>Number of songs</th>
              <th />
            </tr>
          </thead>
          <tbody>{this.playlists()}</tbody>
        </Table>
      </Row>
    );
  }
}

function mapStateToProps(state) {
  return {
    playlists: state.Playlists
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { removePlaylist, newPlaylist, openPlaylist },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Playlists);
