import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import {
  Input,
  FormGroup,
  Label,
  Row,
  Col,
  Table,
  Nav,
  Navbar,
  NavItem
} from "reactstrap";
import Octicon from "react-octicon";
import History from "../components/History";
import Submenu from "../components/Submenu";

const tableStyle = {
  with: "50%"
};

class Artists extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: ""
    };
    this.artistsList = this.artistsList.bind(this);
    this.filterTerm = this.filterTerm.bind(this);
  }

  encodeArtistUrl(url) {
    return encodeURI("/artist/" + url);
  }

  artistsList() {
    return this.filterTerm().map((artist, i) => {
      return (
        <tr key={artist.id}>
          <td className="text-right">
            <Link
              className="artist"
              to={`${this.props.match.url}${this.encodeArtistUrl(
                artist.name
              )}`}>
              {/* <Link className="artist" to={this.encodeArtistUrl(artist.name)}> */}
              {artist.name}
              <Octicon mega name="chevron-right" />
            </Link>
          </td>
          <th scope="row">{i}</th>
        </tr>
      );
    });
  }
  filterTerm() {
    return this.props.artists.filter((artist, index) => {
      return (
        artist.name.toLowerCase().indexOf(this.state.searchTerm.toLowerCase()) >
        -1
      );
    });
  }

  render() {
    if (this.props.artists.length) {
      return (
        <Row>
          <Col>
            <Submenu>
              <NavItem>
                <History
                  backbtnTitle={`Go back to ${this.props.playlist.name} playlist`}
                />
              </NavItem>
            </Submenu>
            <h1> Artists </h1>

            <div className="d-flex justify-content-center flex-row">
              <FormGroup>
                <Label for="ArtistSearch">Search Artist</Label>
                <Input
                  value={this.state.term}
                  onChange={event =>
                    this.setState({ searchTerm: event.target.value })
                  }
                  type="search"
                  name="search"
                  id="ArtistSearch"
                  placeholder="search artist"
                />
              </FormGroup>
            </div>

            <Row>
              <Col />
              <Col>
                <Table style={tableStyle} borderless>
                  <thead>
                    <tr>
                      <th className="text-right">Name</th>
                      <th />
                    </tr>
                  </thead>
                  <tbody>{this.artistsList()}</tbody>
                </Table>
              </Col>
              <Col />
            </Row>
          </Col>
        </Row>
      );
    }
    else {
      return (
        <Row>
          <Col>
            Loading artists list <Octicon mega spin name="sync" />
          </Col>
        </Row>
      )
    }
  }
}

function mapStateToProps(state) {
  return {
    artists: state.Artists,
    playlist: state.Playlist
  };
}

export default connect(mapStateToProps)(Artists);
