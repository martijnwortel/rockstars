import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Route, Switch } from "react-router";
import { Link, withRouter } from "react-router-dom";
import Submenu from "../components/Submenu";

import SongsList from "../components/SongsList";
import NameAndEdit from "../components/NameAndEdit";

import Artists from "./Artists";
import ArtistDetail from "./ArtistDetail";

import {
  Row,
  Col,
  Button,
  NavItem
} from "reactstrap";

import {
  toggleSongInPlaylist,
  changeNamePlaylist,
  savePlaylist,
  openPlaylist
} from "../actions/index";

export class Playlist extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playlistId: -1
    };

    this.removeSong = this.removeSong.bind(this);
    this.afterEditEvent = this.afterEditEvent.bind(this);
    this.savePlaylist = this.savePlaylist.bind(this);
    this.playlistIndex = this.playlistIndex.bind(this);
    this.openPlaylist = this.openPlaylist.bind(this);
  }
  componentWillMount() {
    this.openPlaylist();
  }

  removeSong(data, index) {
    this.props.removeSongFromPlaylist(index);
  }
  afterEditEvent(name) {
    this.props.changeNamePlaylist(this.props.playlist, name);
  }
  savePlaylist() {
    const plIndex = this.playlistIndex();
    this.props.savePlaylist(this.props.playlist, plIndex);
    this.props.history.push("/playlists");
  }
  playlistIndex() {
    let playlistIndex = -1;
    this.props.playlists.forEach((item, index) => {
      if (item.id === this.props.playlist.id) playlistIndex = index;
    });
    return playlistIndex;
  }

  openPlaylist() {
    let playlistIdUrl;
    let playlistFromUrl;

    playlistIdUrl = this.props.match.params.playlist || -1;
    playlistFromUrl = this.props.playlists ?
      this.props.playlists.filter(item => {
        return item.id === playlistIdUrl;
      }) : "";

    if (this.props.playlist.id === -1) {
      if (playlistFromUrl.length === 1) {

        this.props.openPlaylist(playlistFromUrl[0]);
      } else {
        this.props.history.push("/");
      }
    }
  }

  render() {
    return (
      <Switch>
        <Route
          path="/playlist/:playlist/add/artist/:artist"
          component={ArtistDetail}
        />
        <Route path="/playlist/:playlist/add" component={Artists} />
        <Route
          path="/playlist/:playlist"
          render={() => {
            return (
              <Row>
                <Col>
                  <Submenu>
                    <NavItem>
                      <Button outline tag={Link} to="/playlists">
                        Close without save
                      </Button>
                    </NavItem>
                    <NavItem>
                      <Button outline onClick={this.savePlaylist}>
                        Save and close playlist
                      </Button>
                    </NavItem>
                  </Submenu>

                  <NameAndEdit
                    initialName={this.props.playlist.name}
                    afterEditEvent={this.afterEditEvent}
                  />
                  <SongsList
                    songs={this.props.playlist.songs}
                    playlist={this.props.playlist.songs}
                    toggleSongInPlaylist={this.props.toggleSongInPlaylist}
                    showAddSongsBtn={true}
                  />
                </Col>
              </Row>
            );
          }}
        />
      </Switch>
    );
  }
}

export function mapStateToProps(state) {
  return {
    playlist: state.Playlist,
    playlists: state.Playlists
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    { toggleSongInPlaylist, changeNamePlaylist, savePlaylist, openPlaylist },
    dispatch
  );
}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Playlist)
);
