import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";
import SongsList from "../components/SongsList";
import History from "../components/History";
import Submenu from "../components/Submenu";

import {
  setArtistActive,
  setArtistSongs,
  toggleSongInPlaylist
} from "../actions/index";
import { Row, Col, Nav, Navbar, NavItem } from "reactstrap";

const linkBtnStyle = {
  padding: ".4em",
  display: "inline-block"
};

class ArtistDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      artist: ""
    };
    this.setSongs = this.setSongs.bind(this);
  }

  setSongs() {
    this.props.setArtistSongs(
      this.decodeArtistFromUrl(this.props.match.params.artist),
      this.props.songs
    );
  }
  componentWillMount() {
    this.setSongs();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.dbLoaded.songs !== this.props.dbLoaded.songs) {
      this.setSongs();
    }
  }

  decodeArtistFromUrl(artistUrl) {
    return decodeURI(artistUrl);
  }

  render() {
    if (!this.props.dbLoaded.songs) {
      return <div>Laden</div>;
    }
    return (
      <Row>
        <Col>
          <Submenu>
            <NavItem>
              <History backbtnTitle="Find more artists" />
            </NavItem>
            <NavItem>
              <Link
                to={`/playlist/${this.props.match.params.playlist}`}
                style={linkBtnStyle}>
                Go back to {this.props.playlist.name}
              </Link>
            </NavItem>
          </Submenu>

          <div className="text-center">
            <h1>Artist: {this.props.match.params.artist}</h1>
          </div>

          <SongsList
            songs={this.props.artistSongs}
            playlist={this.props.playlist.songs}
            toggleSongInPlaylist={this.props.toggleSongInPlaylist}
            showAddSongsBtn={false}
          />
        </Col>
      </Row>
    );
  }
}

function mapStateToProps(state) {
  return {
    artistSongs: state.ArtistSongs,
    songs: state.Songs,
    playlist: state.Playlist,
    dbLoaded: state.DBLoaded
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setArtistActive,
      setArtistSongs,
      toggleSongInPlaylist
    },
    dispatch
  );
}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ArtistDetail)
);
