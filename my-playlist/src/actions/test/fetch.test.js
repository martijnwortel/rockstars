import {
  API_ROOT_URL
} from "../index";
import {
  fetchData
} from "../index";

const Server = require('../../../db/server');
let testServer;

describe("fetch db", function () {
  beforeAll(() => {
    testServer = Server.startServer();
  });

  it("should fetch", async () => {
    const response = await fetchData(API_ROOT_URL + "/artists");
    expect(response.ok).toBe(true);
  });
  it("should contain artists", async () => {
    const response = await fetchData(API_ROOT_URL + "/artists");
    // console.log(response.json())
    const data = await response.json();
    expect(data.length).toBeGreaterThan(0);
  });
  it("arist should have id and name", async () => {
    const response = await fetchData(API_ROOT_URL + "/artists");
    const data = await response.json();
    expect(data[1].name).not.toBe(undefined);
    expect(data[1].id).not.toBe(undefined);
  });
  afterAll(() => {
    testServer.close(() => {
      console.log("testserver is closed");
    });
  });
});