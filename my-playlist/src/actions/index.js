// const API_ROOT_URL = process.env.API_URL_PROTOCOL + process.env.API_URL_ROOT + process.env.API_URL_PORT;
export const API_ROOT_URL = 'http://localhost:9000';

export const FETCH_ARTISTS = 'FETCH_ARTISTS';
export const FETCH_SONGS = 'FETCH_SONGS';
export const SET_ARTIST_ACTIVE = 'SET_ARTIST_ACTIVE';
export const SET_ARTIST_SONGS = 'SET_ARTIST_SONGS';

export const TOGGLE_SONG_IN_PLAYLIST = 'TOGGLE_SONG_IN_PLAYLIST';
export const CHANGE_PLAYLIST_NAME = 'CHANGE_PLAYLIST_NAME';
export const NEW_PLAYLIST = 'NEW_PLAYLIST';
export const SAVE_PLAYLIST = 'SAVE_PLAYLIST';
export const OPEN_PLAYLIST = 'OPEN_PLAYLIST';
export const REMOVE_PLAYLIST = 'REMOVE_PLAYLIST';

export const FETCH_HAS_ERROR = 'FETCH_HAS_ERROR';
export const APP_HAS_ERROR = 'APP_HAS_ERROR';
export const SET_DB_LOADED = 'SET_DB_LOADED';

export function fetchData(apiUrl, config) {
    const api = apiUrl;
    return fetch(api, config);
}
export function fetchArtists() {
    return (dispatch) => {
        fetchData(
                // API_ROOT_URL + process.env.API_URL, {
                API_ROOT_URL + "\/artists", {
                    credentials: 'include'
                }
            )
            .then((response) => {
                if (response.ok) {
                    return response.json()
                } else {
                    dispatch(fetchHasErrored(true))
                }
            }).then((response) => {
                dispatch({
                    type: FETCH_ARTISTS,
                    payload: response
                })
                dispatch({
                    type: SET_DB_LOADED,
                    payload: {
                        artists: true
                    }
                })
            })
    }
}

export function fetchSongs() {

    return (dispatch) => {
        fetchData(
                // API_ROOT_URL + process.env.API_URL, {
                API_ROOT_URL + "\/songs", {
                    credentials: 'include'
                }
            )
            .then((response) => {
                if (response.ok) {

                    return response.json()
                } else {
                    dispatch(fetchHasErrored(true))
                }
            }).then((response) => {

                dispatch({
                    type: FETCH_SONGS,
                    payload: response
                })
                dispatch({
                    type: SET_DB_LOADED,
                    payload: {
                        songs: true
                    }
                })

            })
    }
}

export function setDBLoaded(loaded) {
    return {
        type: SET_DB_LOADED,
        payload: {
            loaded
        }
    }
}

export function fetchHasErrored(bool) {
    return {
        type: 'FETCH_HAS_ERROR',
        hasErrored: bool
    };
}
export function appInitHasErrored(error) {
    return {
        type: 'APP_HAS_ERROR',
        hasErrored: error
    };
}
export function setArtistActive(name) {
    return {
        type: SET_ARTIST_ACTIVE,
        payload: {
            name
        }
    }
}


export function setArtistSongs(artist, songs) {
    return {
        type: SET_ARTIST_SONGS,
        payload: {
            artist,
            songs
        }
    }
}
export function toggleSongInPlaylist(song, playlistIndex) {
    return {
        type: TOGGLE_SONG_IN_PLAYLIST,
        payload: {
            song,
            playlistIndex
        }
    }
}

export function changeNamePlaylist(playlist, name) {
    return {
        type: CHANGE_PLAYLIST_NAME,
        payload: {
            playlist,
            name
        }
    }
}
export function newPlaylist() {
    return {
        type: NEW_PLAYLIST,
        payload: {

        }
    }
}
export function openPlaylist(playlist) {
    return {
        type: OPEN_PLAYLIST,
        payload: {
            playlist
        }
    }
}
export function savePlaylist(playlist, plIndex) {

    return (dispatch) => {
        dispatch({
            type: SAVE_PLAYLIST,
            payload: {
                playlist,
                plIndex
            }
        })
    }
}

export function removePlaylist(playlistIndex) {
    return {
        type: REMOVE_PLAYLIST,
        payload: {
            playlistIndex
        }
    }
}