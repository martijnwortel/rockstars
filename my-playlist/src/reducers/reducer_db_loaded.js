import {
    SET_DB_LOADED
} from '../actions/index';

const inital = {
    artists: false,
    songs: false
}

export default function(state = inital, action) {
    switch (action.type) {
        case SET_DB_LOADED:
            return Object.assign({}, state, {...action.payload
            })

        default:
            return state
    }
}