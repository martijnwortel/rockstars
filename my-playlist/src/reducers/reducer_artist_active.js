import {
  SET_ARTIST_ACTIVE
} from '../actions/index';

export default function (state = [], action) {
  switch (action.type) {
    case SET_ARTIST_ACTIVE:

      return action.payload
    default:
      return state
  }
}