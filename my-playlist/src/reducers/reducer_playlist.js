import {
    OPEN_PLAYLIST,
    TOGGLE_SONG_IN_PLAYLIST,
    CHANGE_PLAYLIST_NAME
} from '../actions/index';

const initialState = {
    id: -1,
    name: "",
    songs: [],
    saved: false,
}
export default function(state = initialState, action) {
    switch (action.type) {
        case OPEN_PLAYLIST:
            return Object.assign({}, state, action.payload.playlist)
        case TOGGLE_SONG_IN_PLAYLIST:
            const playlistIndex = action.payload.playlistIndex;
            if (playlistIndex > -1) {
                return Object.assign({}, state, {
                    songs: [...state.songs.slice(0, playlistIndex), ...state.songs.slice(playlistIndex + 1)]
                })
            } else {
                return Object.assign({}, state, {
                    songs: [...state.songs, ...[action.payload.song]]
                })
            }
        case CHANGE_PLAYLIST_NAME:
            return Object.assign({}, state, {
                name: action.payload.name
            })
        default:
            return state
    }
}