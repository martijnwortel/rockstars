import {
    SET_ARTIST_SONGS
} from '../actions/index';
import {
    LOCATION_CHANGE
} from 'react-router-redux';

export default function(state = [], action) {
    switch (action.type) {
        case SET_ARTIST_SONGS:
            const artistName = action.payload.artist;
            const songs = action.payload.songs;
            return songs.filter(song => song.artist === artistName)
        case LOCATION_CHANGE:
            return state
        default:
            return state
    }
}