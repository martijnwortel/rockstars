import {
    combineReducers
} from "redux";
import {
    routerReducer
} from "react-router-redux";

import DBLoaded from "./reducer_db_loaded";
import Artists from "./reducer_artists";
import Songs from "./reducer_songs";
import ArtistActive from "./reducer_artist_active";
import ArtistSongs from "./reducer_artist_songs";
import Playlist from "./reducer_playlist";
import Playlists from "./reducer_playlists";

const rootReducer = combineReducers({
    DBLoaded,
    Artists,
    Songs,
    ArtistActive,
    ArtistSongs,
    Playlist,
    Playlists,
    routerReducer
});

export default rootReducer;