import {
    SAVE_PLAYLIST,
    REMOVE_PLAYLIST,
    NEW_PLAYLIST
} from '../actions/index';

import uniqid from 'uniqid';

import {
    playlistTemplate
} from '../playlistTemplate'

export default function(state = [], action) {
    switch (action.type) {
        case NEW_PLAYLIST:
            const playlistLength = state.length;

            return [...state, Object.assign({}, playlistTemplate, {
                id: uniqid(),
                name: "new playlist" + uniqid()
            })]

        case SAVE_PLAYLIST:
            const playlist = action.payload.playlist;
            return state.map(item => {
                if (item.id === playlist.id) {
                    return {...item,
                        ...playlist
                    }
                }
                return item
            })
        case REMOVE_PLAYLIST:
            const playlistIndex = action.payload.playlistIndex;
            return [...state.slice(0, playlistIndex), ...state.slice(playlistIndex + 1)]
        default:
            return state
    }
}