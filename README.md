This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Table of Contents

- [Intro](#intro)
- [Folder Structure](#folder-structure)
- [Available Scripts](#available-scripts)
- [How to start](#how-to-start)

## Intro

My playlist is built with React en Redux
Litte bit of design has been done with Bootstrap 4

## Folder Structure

```
my-playlist/
  README.md
  db/
  src/
  public/
```

## Available Scripts

    "start": 
    "build": 
    "test": unit tests
    "teste2e": one end 2 end test (make 2 new playlists)
    "startdb": "./node_modules/.bin/json-server --port 9000 --watch db/db.json"


## How to start

cd into the my-playlist folder

- run npm install

after installation:

- run npm start

there are 2 different npm scripts for testing

* npm test
* npm run teste2e